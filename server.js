var express = require('express');
var app = express();
var path = require('path');
const port = 3000;

app.listen(port, () => console.log(`SW Demo listening on port ${port}`))

// --ROUTES

// Pages

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/index.html', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/service_worker_1/sw1.html', function(req, res) {
    res.sendFile(path.join(__dirname + '/service_worker_1/sw1.html'));
});

app.get('/service_worker_1/about/about.html', function(req, res) {
    res.sendFile(path.join(__dirname + '/service_worker_1/about/about.html'));
});

app.get('/service_worker_2/sw2.html', function(req, res) {
    res.sendFile(path.join(__dirname + '/service_worker_2/sw2.html'));
});

app.get('/service_worker_3/sw3.html', function(req, res) {
    res.sendFile(path.join(__dirname + '/service_worker_3/sw3.html'));
});

app.get('/service_worker_sync/sync.html', function(req, res) {
    res.sendFile(path.join(__dirname + '/service_worker_sync/sync.html'));
});


// CSS

app.get('/css/style.css', function(req, res) {
    res.sendFile(path.join(__dirname + '/css/style.css'));
});

// JS

app.get('/sw1.js', function(req, res) {
    res.sendFile(path.join(__dirname + '/sw1.js'));
});

app.get('/sw2.js', function(req, res) {
    res.sendFile(path.join(__dirname + '/sw2.js'));
});

app.get('/service_worker_1/about/sw3.js', function(req, res) {
    res.sendFile(path.join(__dirname + '/service_worker_1/about/sw3.js'));
});

app.get('/js/register_sw1.js', function(req, res) {
    res.sendFile(path.join(__dirname + '/js/register_sw1.js'));
});

app.get('/js/register_sw2.js', function(req, res) {
    res.sendFile(path.join(__dirname + '/js/register_sw2.js'));
});

app.get('/js/register_sw3.js', function(req, res) {
    res.sendFile(path.join(__dirname + '/js/register_sw3.js'));
});

app.get('/sync.js', function(req, res) {
    res.sendFile(path.join(__dirname + '/sync.js'));
});

app.get('/js/register_sync.js', function(req, res) {
    res.sendFile(path.join(__dirname + '/js/register_sync.js'));
});
