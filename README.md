# Service Workers Demo

Service workers are basically a proxy between your web application and the internet. They enable applications to control network requests, cache those requests to improve performance, and provide offline access to cached content.
During development we'll be able to use service workers through localhost, but to deploy it on a site you'll need to have HTTPS setup on your server.

This demo works with a basic Express server in Node.js. To run this demo, in your terminal, run the command 'node server.js' in the root of sw_demo and go to http://localhost:3000/.

# lifecycle of a Service worker

- Register

To install a service worker, you need to register it, which is in this example done in the main.js file.
Registering a service worker tells the browser where your service worker is located and will cause the browser to start the service worker install step in the background.

The code starts by checking for browser support by examining navigator.serviceWorker. The service worker is then registered with navigator.serviceWorker.register, which returns a promise that resolves when the service worker has been successfully registered. The scope of the service worker is then logged with registration.scope. For more information about the scoping property please continue to section scoping. If the service worker is already installed, navigator.serviceWorker.register returns the registration object of the currently active service worker.

So if we run the app and navigate to sw1 or sw2, we can see in the console that our service workers register, install and activate. After we refresh the page, there will only be a return of a registration object because the service worker is allready installed.

- Install

Once the the browser registers a service worker, installation can be attempted. This occurs if the service worker is considered to be new by the browser, either because the site currently doesn't have a registered service worker, or because there is a byte difference between the new service worker and the previously installed one.

A service worker installation triggers an install event in the installing service worker. We can include an install event listener in the service worker to perform some task when the service worker installs. For instance, during the install, service workers can precache parts of a web app so that it loads instantly the next time a user opens it. So, after that first load, you're going to benefit from instant repeat loads and your time to interactivity is going to be even better in those cases.

So typically during the install step, you'll want to cache some static assets. If all the files are cached successfully, then the service worker becomes installed. If any of the files fail to download and cache, then the install step will fail and the service worker won't activate (i.e. won't be installed). If that happens, don't worry, it'll try again next time. But that means if it 
does install, you know you've got those static assets in the cache.

self.skipWaiting():

The skipWaiting() method of the ServiceWorkerGlobalScope forces the waiting service worker to become the active service worker.
While self.skipWaiting() can be called at any point during the service worker's execution,
it will only have an effect if there's a newly installed service worker that might otherwise remain in the waiting state.

- Activation

Once a service worker has successfully installed, it transitions into the activation stage. If there are any open pages controlled by the previous service worker, the new service worker enters a waiting state. The new service worker only activates when there are no longer any pages loaded that are still using the old service worker. This ensures that only one version of the service worker is running at any given time.

The activation step is a great opportunity for handling any management of old caches. for example sw1.js wil update the cached assets and sw2.js will delete all cache.

Once activated, the service worker controls all pages that load within its scope, and starts listening for events from those pages.
!! However, pages in your app that were loaded before the service worker activation will not be under service worker control. The new service worker will only take over when you close and reopen your app. Until then, requests from this page will not be intercepted by the new service worker. This is intentional as a way to ensure consistency in your site.

# FetchEvent and Lifecycle

- Fetch

After a service worker is installed and the user navigates to a different page or refreshes, the service worker will begin to receive fetch events (see also example in sw1.js).
In this example we've defined our fetch event and within event.respondWith(), we pass in a promise from caches.match(). This method looks at the request and finds any cached results from any of the caches your service worker created.
If we have a matching response, we return the cached value, otherwise we return the result of a call to fetch, which will make a network request and return the data if anything can be retrieved from the network. This is a simple example and uses any cached assets we cached during the install step.

- Lifecycle

The FetchEvent lifecycle starts when Cloudflare’s edge network receives a request whose URL matches both a zone and a route for a Workers function; this causes the Workers runtime to trigger a fetch event and creates a FetchEvent Object to pass to the first event handler in the Workers function registered for 'fetch'. Then the event handler can use any of the following to control what happens next:

- RespondWith()

Intercepts the request and allows users to send a custom response.

If a fetch event handler does not call respondWith(), the runtime delivers the event to the next registered fetch event handler. If no event handler calls respondWith(), the runtime proxies the request to the origin. Note: If no origin is setup, then you must have a respondWith() called for a valid response.

- WaitUntil()

Extends the lifetime of the event using a Promise passed into the function. Use this method to notify the runtime to wait for tasks, such as streaming and caching, that run longer than the usual time it takes to send a response. This is good for handling logging and analytics to third-party services, where you don’t want to block the response.

# Scoping

Scope refers to the path that the service worker will be able to intercept network calls from.
The scope property can be used explicitly to define the scope it will cover and is set when registering a new service worker (main.js).
!! However, Service workers can only intercept requests originating in the scope of the current directory that the service worker script is located in and its subdirectories.

In this example the service workers (sw1, sw2) are set in the root of SW_DEMO. this should mean that they can intercept all requests for every origin on the domain. However, their scope is defined differently so only requests on this scope will be intercepted.
Service worker 3 is an example of a faulty scoped service worker. So when you navigate to SW 3 you should see an error in the console explaining that the scope is NOT under the max scope allowed.
sw3.js current directory is /service_worker-1/about/sw3.js

# Background sync and notification

The lifecycle with background sync is slightly different. First a user makes a request, but instead of the request being attempted immediately, the service worker steps in.
The service worker will check if the user has internet access and if they do, great. The request will be sent. If not, the service worker will wait until the user does have internet and at that point send the request, after it fetches data out of IndexedDB.
Best of all, background sync will go ahead and send the request even if the user has navigated away from the original page.

- register_sync.js:

First we use Dexie.js as a wrapper library for indexedDB (the standard database in the browser).
Here we call 'sw_demo' as our database name and create a schema named contacts with id as primary key and name as value key.

Then the user has a choice to allow permission by the browser for sending notifications (doesn't work in anonymous window).

After that we are registering the service worker like before, but now we're taking advantage of the fact that the register function returns a promise.
The next piece you see is navigator.serviceWorker.ready. This is a read-only property of a service worker that essentially just lets you know if the service worker is ready or not.
This property provides a way for us to delay execution of the following functions until the service worker is actually ready.

Next we have a reference to the service worker's registration. We'll put an event listener on our submit button, and save the name values in the contacts schema of indexedDB.
Then we register a sync event and pass in a string. That string will be used over on the service worker side later on.

- sync.js:

We attach a function to onsync, the event listener for background sync. We want to watch for the string we passed into the register function back in register_sync.js.
We're watching for that string using event.tag.

We're also using event.waitUntil. Because a service worker isn't continually running -- it "wakes up" to do a task and then "goes back to sleep" -- we want to use event.waitUntil to keep the service worker active.
This function accepts a function parameter. The function we pass in will return a promise, and event.waitUntil will keep the service worker "awake" until that function resolves.
If we didn't use event.waitUntil the request might never make it to the server because the service worker would run the onsync function and then immediately go back to sleep.

The background sync will check on the status of the user's internet connection and sends the request again if the first attempt fails!

IndexedDB utilizes callbacks while a service worker is promise-based, so we'll have to account for that in our function.
Walking through it, we're returning a promise, and we'll use the resolve and reject parameters to make this function more promise-based to keep everything in line with the service worker.

We'll open our database and we'll use the getAll method to pull all the data from the specified object store. Once that is success, we'll resolve the function with the data.
If we have an error, we'll reject. This keeps our error handling working the same way as all of the other promises and makes sure we have the data before we send it off to the server.

After we get the data, we just make a fetch request the way we normally would.

Now that our promise is finished, we can show a notification telling the user the data was send to the server.

Give it a try en go to the SYNC page. Allow notifications by clicking the 'Allow Notifications' button. Turn off your internet connection and try sending names.
Turn your internet connection back on and a notification will be shown. Also when you open the console, you can check your names send to the server.

# Foreign fetch

!! Not enabled by this time

What if a third-party provider of an API, or web fonts, or other commonly used service had the power to deploy their own service worker that got a chance to handle requests made by other origins to their origin?
Providers could implement their own custom networking logic, and take advantage of a single, authoritative cache instance for storing their responses.
Now, thanks to foreign fetch, that type of third-party service worker deployment is a reality.

Foreign fetch is still considered experimental. In order to keep from prematurely baking this design in before it’s fully specified and agreed upon by browser vendors, it's been implemented in Chrome 54 as an Origin Trial.
As long as foreign fetch remains experimental, to use this new feature with the service you host, you’ll need to request a token that's scoped to your service's specific origin.
The token should be included as an HTTP response header in all cross-origin requests for resources that you want to handle via foreign fetch, as well as in the response for your service worker JavaScript resource:

Origin-Trial: token_obtained_from_signup

The trial will end in March 2017. By that point, we expect to have figured out any changes necessary to stabilize the feature, and (hopefully) enable it by default.
If foreign fetch is not enabled by default by that time, the functionality tied to existing Origin Trial tokens will stop working.
