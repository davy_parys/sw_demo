const cacheName = 'v3';

self.addEventListener('install', e => {
  console.log('Service Worker 3 installed');
});

// Call Activate Event
self.addEventListener('activate', e => {
  console.log('Service Worker 3: Activated');
});
