

if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {

      // SW 2 Registration
      navigator.serviceWorker
      .register('../sw2.js', {scope: '/service_worker_2/'})
      .then(reg => console.log('Service Worker 2: Registered', reg))
      .catch(err => console.log(`Service Worker 2: Error:`, err));

    })
}