
// Register Service Workers

// Make sure sw are supported in browser
if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {

    // SW 1 Registration
    navigator.serviceWorker
      .register('../sw1.js', {scope: '/service_worker_1/'}) // In this case we are setting the scope of the service worker to /service_worker_1/,
                                                            // which means the service worker will control requests from pages like /service_worker_1/, /service_worker_1/lower/ and /service_worker_1/lower/lower,
                                                            // but not from pages like /service_worker_1 or /, which are higher.
      .then(reg => console.log('Service Worker 1: Registered', reg))
      .catch(err => console.log(`Service Worker 1: Error:`, err));


    // It's possible to register an arbitrary number of service workers for a given origin, as long as each service worker has its own unique scope.
    // SW3 in this instance is disabled because it has the same scope as SW 1.

      // SW 3 Registration
    // navigator.serviceWorker
    //   .register('../service_worker_1/sw3.js')
    //   .then(reg => console.log('Service Worker 3: Registered', reg))
    //   .catch(err => console.log(`Service Worker 3: Error:`, err));

    
  })
}