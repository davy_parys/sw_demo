


if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {

        // SW 3 Registration
        navigator.serviceWorker
            .register('../service_worker_1/about/sw3.js', {scope: '/service_worker_3/'})
            .then(reg => console.log('Service Worker 3: Registered', reg))
            .catch(err => console.log(`Service Worker 3: Error:`, err));
    })
}
