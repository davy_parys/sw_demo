
// Read section 'Background sync and notification' in README.md for guidance.

const db = new Dexie('sw_demo');
db.version(1).stores({
    contacts: 'id, name'
});

let perm = document.getElementById("perm");

perm.addEventListener("click", function () {
    Notification.requestPermission().then(permission => {
        if (permission === 'granted') console.log("permission granted");
        else console.error("Permission was not granted.");
    })
});

    // SW SYNC Registration
    if(navigator.serviceWorker) {
        navigator.serviceWorker.register('../sync.js')
            .then(function() {
                return navigator.serviceWorker.ready // check if service worker is finished installing and activating
            })
            .then(function(registration) {
                let i = 1;
                document.getElementById('submitForm').addEventListener('click', (event) => {
                    let name = document.getElementById("name").value;
                    db.contacts.put({id: i, name: name});
                    i++;
                    if(registration.sync) {
                        registration.sync.register('example-sync')
                            .catch(function(err) {
                                return err;
                            })
                    }
                })
            })
    }
