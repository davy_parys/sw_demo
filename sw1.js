const cacheName = 'v1';

const cacheAssets = [
  'index.html',
  '/service_worker_1/sw1.html',
  '/service_worker_1/about/about.html',
  '/service_worker_2/sw2.html',
  '/css/style.css',
];

// --INSTALL EVENT

// The install event only fires when the service worker file is found to be new - either different to an existing service worker, or the first service worker encountered for this page/site.
// It is good practice to cache any static files (cacheAssets) that your application may need, this means your application won't have to download these files unless they are updated.

// The install event use waitUntil() to hold the service worker in the installing phase until tasks complete.
// If the promise passed to waitUntil() rejects, the install is considered a failure, and the installing service worker is discarded.
// This is primarily used to ensure that a service worker is not considered installed until all of the core caches it depends on are successfully populated.


// Therefore, it's common to call self.skipWaiting() from inside of an InstallEvent handler.

// The following example causes a newly installed service worker to progress into the activating state, regardless of whether there is already an active service worker.

self.addEventListener('install', e => {
  console.log('Service Worker 1 installed');

  e.waitUntil(
    caches
      .open(cacheName)
        .then(cache => {
          console.log('Service Worker 1: Caching Files');
          console.log(cache);
          cache.addAll(cacheAssets);
      })
      .then(() => self.skipWaiting()) // The skipWaiting() method of the ServiceWorkerGlobalScope forces the waiting service worker to become the active service worker.
  );
});

// --ACTIVATE EVENT

// The activate event will fire every time you make connection to your service worker.

// The activate event use waitUntil() to buffer functional events such as fetch and push until the promise passed to waitUntil() settles.
// This gives the service worker time to update database schemas and delete outdated caches, so other events can rely on a completely upgraded state.

self.addEventListener('activate', e => {
  console.log('Service Worker 1: Activated');
  // Remove unwanted caches
  e.waitUntil(
    caches.keys().then(keys => {
      return Promise.all(
        keys.map(key => {
          if (key !== cacheName) {
            console.log('Service Worker 1: Clearing Old Cache');
            return caches.delete(key);
          }
        })
      );
    })
  );
});

// --FETCH EVENT

// A fetch event fires every time any resource controlled by a service worker is fetched, which includes the documents inside the specified scope,
// and any resources referenced in those documents (for example if index.html makes a cross origin request to embed an image,
// that still goes through its service worker.)
// Give it a try en run the sw1 page offline.

self.addEventListener('fetch', e => {
  console.log('Service Worker 1: Fetching on http://127.0.0.1:5500/service_worker_1');
  e.respondWith(
    fetch(e.request) // Check if there is any internet connection
      .catch(() => caches.match(e.request))); // If it fails, get the matched cache request
});