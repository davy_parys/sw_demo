

// Read section 'Background sync and notification' in README.md for guidance.

self.onsync = function(event) {
    if(event.tag === 'example-sync') {
        event.waitUntil(sendToServer()
            .then(() => {
                self.registration.showNotification("Synced to server");
            })
            .catch(() => {
                console.log("Error syncing to server");
            })
        );
    }
};

function sendToServer(){
    return new Promise(function(resolve, reject) {
        let db = indexedDB.open('sw_demo');
        db.onsuccess = function(event) {
            this.result.transaction("contacts").objectStore("contacts").getAll().onsuccess = function(event) {
                resolve(event.target.result);
                let response = event.target.result;
                console.log(JSON.stringify(response));
                fetch('https://www.mocky.io/v2/5c0452da3300005100d01d1f', {
                    method: 'POST',
                    body: JSON.stringify(response),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                })
            };
        }
        db.onerror = function (err) {
            reject(err);
        }
    });
}
